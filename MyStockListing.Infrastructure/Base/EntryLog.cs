﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MyStockListing.Infrastructure.Base
{
    public class EntryLog
    {
        protected EntryLog() { }

        [Key]
        public Guid Id { get; private set; }

        public string ChangeSet { get; private set; }

        public string EventName { get; private set; }

        public DateTime OccuredAt { get; private set; }

        public EntryLog(Guid id, string eventName, object changeSet, DateTime occuredAt)
        {
            // Definitely not the best way to store events in the database but gets the job done for now/future.
            this.Id = id;
            this.EventName = eventName;
            this.OccuredAt = occuredAt;
            this.ChangeSet = JsonConvert.SerializeObject(changeSet, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }
}
