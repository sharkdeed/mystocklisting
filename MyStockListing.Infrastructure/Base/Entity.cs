﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyStockListing.Infrastructure.Base
{
    public abstract class Entity
    {
        protected Entity() { }

        public HashSet<EntryLog> Entries { get; protected set; } = new HashSet<EntryLog>();

        public Entity(Guid id)
        {
            Id = id;
            CreatedAt = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime? UpdatedAt { get; protected set; }

        public DateTime? DeletedAt { get; protected set; }

        public virtual void Update()
        {
            UpdatedAt = DateTime.UtcNow;
        }

        public virtual void Delete()
        {
            DeletedAt = DateTime.UtcNow;
        }
    }
}
