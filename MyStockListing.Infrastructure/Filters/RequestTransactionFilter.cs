﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace MyStockListing.Infrastructure.Filters
{
    public sealed class RequestTransactionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            DbContext dbContext = (DbContext)context.HttpContext.RequestServices.GetService(typeof(DbContext));
            try
            {
                dbContext.SaveChanges();
                dbContext.Database.CommitTransaction();
            }
            catch
            {
                dbContext.Database.RollbackTransaction();
                throw;
            }
        }

        public async void OnActionExecuting(ActionExecutingContext context)
        {
            DbContext dbContext = (DbContext)context.HttpContext.RequestServices.GetService(typeof(DbContext));
            if (dbContext.Database.CurrentTransaction == null)
            {
                await dbContext.Database.BeginTransactionAsync();
            }
        }
    }
}
