# Requirements

A Working Microsoft SQL Server Instance.

# How to Run

* Create a database called `mystocklisting-db` in your MSSQL Instance.
* Fix the connection string in `Startup.cs:38` according to your MSSQL Instance.
* Run project with Microsoft Visual Studio 2015 or above.