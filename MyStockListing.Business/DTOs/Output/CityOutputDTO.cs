﻿using MyStockListing.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Business.DTOs.Output
{
    public class CityOutputDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string AreaCode { get; set; }

        public CountryOutputDTO Country { get; set; }

        public List<DistrictOutputDTO> Districts { get; set; }

        public static CityOutputDTO FromEntity(City city)
        {
            return new CityOutputDTO
            {
                Id = city.Id,
                Name = city.Name,
                ShortName = city.ShortName,
                AreaCode = city.AreaCode,
                Districts = city.Districts.Select(x => DistrictOutputDTO.FromEntity(x)).ToList()
            };
        }
    }
}
