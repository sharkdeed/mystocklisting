﻿using MyStockListing.Data.Models;
using MyStockListing.Data.ValueObjects;

namespace MyStockListing.Business.DTOs.Output
{
    public class LocationOutputDTO
    {
        public DistrictOutputDTO District { get; set; }

        public Coordinates Coordinates { get; set; }

        public static LocationOutputDTO FromEntity(Location location)
        {
            return new LocationOutputDTO
            {
                District = DistrictOutputDTO.FromEntity(location.District),
                Coordinates = new Coordinates(location.Coordinates.Y, location.Coordinates.X)
            };
        }
    }
}
