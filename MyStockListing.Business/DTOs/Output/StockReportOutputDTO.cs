﻿using System.Linq;

namespace MyStockListing.Business.DTOs.Output
{
    public class StockReportOutputDTO
    {
        public string SKU { get; set; }

        public string Brand { get; set; }

        public string Name { get; set; }

        public string Model { get; set; }

        public int? TotalAmount { get; set; }

        public string Warehouse { get; set; }
    }
}
