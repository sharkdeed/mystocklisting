﻿using MyStockListing.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MyStockListing.Business.DTOs.Output
{
    public class CountryOutputDTO
    {
        public static Expression<Func<Country, CountryOutputDTO>> Projection => x => new CountryOutputDTO
        {
            Name = x.Name,
            ShortName = x.ShortName
        };

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public List<CityOutputDTO> Cities { get; set; }

        public static CountryOutputDTO FromEntity(Country country)
        {
            return new CountryOutputDTO
            {
                Id = country.Id,
                Name = country.Name,
                ShortName = country.ShortName,
                Cities = country.Cities.Select(x => CityOutputDTO.FromEntity(x)).ToList()
            };
        }
    }
}
