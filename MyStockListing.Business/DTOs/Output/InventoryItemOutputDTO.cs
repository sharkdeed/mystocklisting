﻿using MyStockListing.Data.Models;
using System;
using System.Linq.Expressions;

namespace MyStockListing.Business.DTOs.Output
{
    public class InventoryItemOutputDTO
    {
        public static Expression<Func<InventoryItem, InventoryItemOutputDTO>> Projection
        {
            get => x => new InventoryItemOutputDTO
            {
                Id = x.Id,
                Amount = x.Amount,
                Product = new ProductOutputDTO
                {
                    Id = x.ProductId,
                    Brand = x.Product.Brand,
                    Model = x.Product.Model,
                    SKU = x.Product.SKU,
                    Name = x.Product.Name
                },
                Warehouse = new WarehouseOutputDTO
                {
                    Id = x.Warehouse.Id,
                    Name = x.Warehouse.Name
                }
            };
        }

        public Guid Id { get; set; }

        public int? Amount { get; set; }

        public string WarehouseName => this.Warehouse.Name;

        public string ProductName => this.Product.Name;

        public string Model => this.Product.Model;

        public string SKU => this.Product.SKU;

        public Guid WarehouseId => this.Warehouse.Id;

        public Guid ProductId => this.Product.Id;

        public ProductOutputDTO Product { get; set; }

        public WarehouseOutputDTO Warehouse { get; set; }

        public static InventoryItemOutputDTO FromEntity(InventoryItem inventoryItem)
        {
            return new InventoryItemOutputDTO
            {
                Amount = inventoryItem.Amount,
                Product = ProductOutputDTO.FromEntity(inventoryItem.Product),
                Id = inventoryItem.Id,
                Warehouse = WarehouseOutputDTO.FromEntity(inventoryItem.Warehouse)
            };
        }
    }
}
