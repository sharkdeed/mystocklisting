﻿using MyStockListing.Data.Models;
using System;
using System.Linq.Expressions;

namespace MyStockListing.Business.DTOs.Output
{
    public class WarehouseOutputDTO
    {
        public static Expression<Func<Warehouse, WarehouseOutputDTO>> Projection
        {
            get => x => new WarehouseOutputDTO
            {
                Id = x.Id,
                Name = x.Name,
                Location = new LocationOutputDTO
                {
                    District = new DistrictOutputDTO
                    {
                        Id = x.Location.District.Id,
                        Name = x.Location.District.Name,
                        PostalCode = x.Location.District.PostalCode,
                        City = new CityOutputDTO
                        {
                            Id = x.Location.District.City.Id,
                            Name = x.Location.District.City.Name,
                            ShortName = x.Location.District.City.ShortName,
                            Country = new CountryOutputDTO
                            {
                                Id = x.Location.District.City.Country.Id,
                                Name = x.Location.District.City.Country.Name,
                                ShortName = x.Location.District.City.Country.ShortName
                            }
                        }
                    },
                    Coordinates = new Data.ValueObjects.Coordinates(x.Location.Coordinates.Y, x.Location.Coordinates.X)
                }
            };
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Address => $"{Location?.District?.Name}, {Location?.District?.City?.Name}, {Location?.District?.City?.Country?.Name}";

        public string Coordinates => $"{Location?.Coordinates?.Latitude}, {Location?.Coordinates?.Longitude}";

        public LocationOutputDTO Location { get; set; }

        public static WarehouseOutputDTO FromEntity(Warehouse warehouse)
        {
            return new WarehouseOutputDTO
            {
                Id = warehouse.Id,
                Name = warehouse.Name,
                Location = warehouse.Location != null
                    ? new LocationOutputDTO
                    {
                        Coordinates = new Data.ValueObjects.Coordinates(warehouse.Location.Coordinates.Y, warehouse.Location.Coordinates.X)
                    }
                    : null
            };
        }
    }
}
