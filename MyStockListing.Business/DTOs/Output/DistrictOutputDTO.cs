﻿using MyStockListing.Data.Models;
using System;
using System.Linq.Expressions;

namespace MyStockListing.Business.DTOs.Output
{
    public class DistrictOutputDTO
    {
        public static Expression<Func<District, DistrictOutputDTO>> Projection => x => new DistrictOutputDTO
        {
            Id = x.Id,
            Name = x.Name,
            PostalCode = x.PostalCode
        };

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PostalCode { get; set; }

        public CityOutputDTO City { get; set; }

        public static DistrictOutputDTO FromEntity(District district)
        {
            return new DistrictOutputDTO
            {
                Id = district.Id,
                Name = district.Name,
                PostalCode = district.PostalCode
            };
        }
    }
}
