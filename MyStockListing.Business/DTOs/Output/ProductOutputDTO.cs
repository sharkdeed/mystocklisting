﻿using MyStockListing.Data.Models;
using System;
using System.Linq.Expressions;

namespace MyStockListing.Business.DTOs.Output
{
    public class ProductOutputDTO
    {
        public static Expression<Func<Product, ProductOutputDTO>> Projection
        {
            get => x => new ProductOutputDTO
            {
                Id = x.Id,
                Brand = x.Brand,
                SKU = x.SKU,
                Name = x.Name,
                Model = x.Model,
                CreatedAt = x.CreatedAt
            };
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string SKU { get; set; }

        public DateTime CreatedAt { get; set; }

        public static ProductOutputDTO FromEntity(Product entity)
        {
            return new ProductOutputDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Brand = entity.Brand,
                Model = entity.Model,
                SKU = entity.SKU,
                CreatedAt = entity.CreatedAt
            };
        }
    }
}
