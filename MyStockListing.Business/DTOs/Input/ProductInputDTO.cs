﻿using System.ComponentModel.DataAnnotations;

namespace MyStockListing.Business.DTOs.Input
{
    public class ProductInputDTO
    {
        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Brand { get; set; }

        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Model { get; set; }

        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string SKU { get; set; }
    }
}
