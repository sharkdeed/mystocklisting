﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyStockListing.Business.DTOs.Input
{
    public class InventoryItemInputDTO
    {
        public int? Amount { get; set; }

        [Required]
        public Guid ProductId { get; set; }

        [Required]
        public Guid WarehouseId { get; set; }
    }

}
