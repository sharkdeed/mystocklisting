﻿using MyStockListing.Data.ValueObjects;
using System;
using System.ComponentModel.DataAnnotations;

namespace MyStockListing.Business.DTOs.Input
{
    public class WarehouseInputDTO
    {
        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        public Guid DistrictId { get; set; }

        [Required]
        [Range(double.MinValue, double.MaxValue)]
        public double Latitude { get; set; }

        [Required]
        [Range(double.MinValue, double.MaxValue)]
        public double Longitude { get; set; }

        public Coordinates Coordinates => new Coordinates(this.Latitude, this.Longitude);
    }
}
