﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Data.Models;
using MyStockListing.Data.Repositories;
using System;
using System.Linq;

namespace MyStockListing.Business.Services
{
    public class LocationService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IGenericRepository _genericRepository;

        public LocationService(IGenericRepository genericRepository, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _genericRepository = genericRepository;
        }

        public IQueryable<CountryOutputDTO> GetAllCountries()
        {
            return _memoryCache.GetOrCreate("Countries", entry => _genericRepository
                .GetAll<Country>()
                .Include(x => x.Cities)
                .ThenInclude(x => x.Districts)
                .Select(x => CountryOutputDTO.FromEntity(x)).ToList()).AsQueryable();
        }

        public IQueryable<DistrictOutputDTO> GetDistrictsByCityId(Guid cityId)
        {
            return _memoryCache
                .GetOrCreate("Districts", entry => _genericRepository.GetAll<District>().ToList())
                .AsQueryable()
                .Where(x => x.CityId == cityId).Select(DistrictOutputDTO.Projection);
        }
    }
}
