﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.EntityFrameworkCore;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Data.Models;
using MyStockListing.Data.Repositories;
using NetTopologySuite.Geometries;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Business.Services
{
    public sealed class WarehouseService
    {
        private readonly IGenericRepository _genericRepository;

        public WarehouseService(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public IQueryable<WarehouseOutputDTO> GetWarehouses(DynamicQueryOptions options, double longitude, double latitude)
        {
            var allSet = _genericRepository.GetAll<Warehouse>();
            var point = new Point(longitude, latitude)
            {
                // spatial reference system id - maybe we can make a factory for this
                SRID = 4326
            };

            int tenKm = 10 * 1000;
            if (latitude > 0 && longitude > 0)
            {
                allSet = allSet.Where(x => x.Location.Coordinates.Distance(point) <= tenKm);
            }

            return allSet.Select(WarehouseOutputDTO.Projection).ApplyFilters(options);
        }

        public async Task<WarehouseOutputDTO> GetWarehouseByIdAsync(Guid warehouseId, CancellationToken cancellationToken)
        {
            return await _genericRepository
                .GetAll<Warehouse>()
                .Select(WarehouseOutputDTO.Projection)
                .FirstOrDefaultAsync(x => x.Id == warehouseId);
        }

        public async Task<WarehouseOutputDTO> CreateWarehouseAsync(WarehouseInputDTO warehouseInputDTO, CancellationToken cancellationToken)
        {
            District district = await _genericRepository
                .GetAll<District>()
                .FirstOrDefaultAsync(x => x.Id == warehouseInputDTO.DistrictId, cancellationToken);

            var location = new Location(Guid.NewGuid(), district, warehouseInputDTO.Coordinates);
            await _genericRepository.AddAsync(location, cancellationToken, true);

            var warehouse = new Warehouse(
                Guid.NewGuid(),
                location,
                warehouseInputDTO.Name);

            await _genericRepository.AddAsync(warehouse, cancellationToken, true);
            return WarehouseOutputDTO.FromEntity(warehouse);
        }

        public async Task<WarehouseOutputDTO> UpdateWarehouseAsync(
            Guid warehouseId,
            WarehouseInputDTO warehouseInputDTO,
            CancellationToken cancellationToken)
        {
            Warehouse warehouse = await FindWarehouseOrThrow(warehouseId, cancellationToken);
            if (warehouseInputDTO.Name != warehouse.Name)
            {
                warehouse.ChangeName(warehouseInputDTO.Name);
            }

            await _genericRepository.UpdateAsync(warehouse, cancellationToken, true);
            return WarehouseOutputDTO.FromEntity(warehouse);
        }

        public async Task<bool> DeleteProductAsync(Guid warehouseId, bool isHardDelete, CancellationToken cancellationToken)
        {
            Warehouse warehouse = await FindWarehouseOrThrow(warehouseId, cancellationToken);
            warehouse.Delete();
            await _genericRepository.UpdateAsync(warehouse, cancellationToken, true);
            return true;
        }

        private async Task<Warehouse> FindWarehouseOrThrow(Guid warehouseId, CancellationToken cancellationToken)
        {
            Warehouse warehouse = await _genericRepository.GetByIdAsync<Warehouse>(warehouseId, cancellationToken);
            if (warehouse == null)
            {
                // not found exception
                throw new InvalidOperationException("");
            }

            return warehouse;
        }
    }
}
