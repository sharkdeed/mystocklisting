﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.Extensions.Caching.Memory;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Data.Models;
using MyStockListing.Data.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Business.Services
{
    public class InventoryItemService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IGenericRepository _genericRepository;

        private const string CACHE_KEY = nameof(InventoryItem);
        private const string REPORT_CACHE_KEY = CACHE_KEY + "Report";

        public InventoryItemService(IGenericRepository genericRepository, IMemoryCache memoryCache)
        {
            _genericRepository = genericRepository;
            _memoryCache = memoryCache;
        }

        public IQueryable<InventoryItemOutputDTO> GetInventoryItems(DynamicQueryOptions options)
        {
            return _memoryCache.GetOrCreate(CACHE_KEY, entry => _genericRepository
                .GetAll<InventoryItem>()
                .Select(InventoryItemOutputDTO.Projection)
                .ApplyFilters(options).ToList())
                .AsQueryable();
        }

        public IQueryable<StockReportOutputDTO> GetStockReport()
        {
            var results = _genericRepository
                .GetAll<InventoryItem>()
                .GroupBy(x => new
                {
                    x.Product.SKU,
                    x.Product.Brand,
                    x.Product.Model,
                    ProductName = x.Product.Name,
                    WarehouseName = x.Warehouse.Name,
                })
                .Select(x => new StockReportOutputDTO
                {
                    TotalAmount = x.Sum(y => y.Amount.GetValueOrDefault()),
                    Brand = x.Key.Brand,
                    Model = x.Key.Model,
                    SKU = x.Key.SKU,
                    Name = x.Key.ProductName,
                    Warehouse = x.Key.WarehouseName
                });

            return _memoryCache.GetOrCreate(REPORT_CACHE_KEY, entry => results.ToList()).AsQueryable();
        }

        public async Task<InventoryItemOutputDTO> GetInventoryItemByIdAsync(Guid inventoryItemId, CancellationToken cancellationToken)
        {
            InventoryItem inventoryItem = await FindInventoryItemOrThrow(inventoryItemId, cancellationToken, true);
            return InventoryItemOutputDTO.FromEntity(inventoryItem);
        }

        public async Task<InventoryItemOutputDTO> CreateInventoryItemAsync(InventoryItemInputDTO inventoryItemInputDTO, CancellationToken cancellationToken)
        {
            Product product = await _genericRepository.GetByIdAsync<Product>(
                inventoryItemInputDTO.ProductId,
                cancellationToken);

            if (product == null)
            {
                throw new InvalidOperationException("Product was not found");
            }

            Warehouse warehouse = await _genericRepository.GetByIdAsync<Warehouse>(inventoryItemInputDTO.WarehouseId, cancellationToken);
            if (warehouse == null)
            {
                throw new InvalidOperationException("Warehouse was not found");
            }

            var inventoryItem = new InventoryItem(
                Guid.NewGuid(),
                inventoryItemInputDTO.Amount,
                product,
                warehouse);

            await _genericRepository.AddAsync(inventoryItem, cancellationToken, true);

            InvalidateCache();
            return InventoryItemOutputDTO.FromEntity(inventoryItem);
        }

        public async Task<bool> UpdateInventoryItem(Guid inventoryItemid, InventoryItemInputDTO inventoryItemInputDTO, CancellationToken cancellationToken)
        {
            InventoryItem inventoryItem = await FindInventoryItemOrThrow(inventoryItemid, cancellationToken, false);
            if (inventoryItemInputDTO.Amount.HasValue && inventoryItemInputDTO.Amount != inventoryItem.Amount)
            {
                inventoryItem.Adjust(inventoryItemInputDTO.Amount.Value);
            }

            if (inventoryItemInputDTO.WarehouseId != inventoryItem.WarehouseId)
            {
                Warehouse movedWarehouse = await _genericRepository.GetByIdAsync<Warehouse>(inventoryItemInputDTO.WarehouseId, cancellationToken);
                inventoryItem.MoveToNewWarehouse(movedWarehouse);
            }

            InvalidateCache();
            return await _genericRepository.UpdateAsync(inventoryItem, cancellationToken, true) > 0;
        }

        public async Task<bool> DeleteProductAsync(Guid inventoryItemid, bool isHardDelete, CancellationToken cancellationToken)
        {
            InventoryItem inventoryItem = await FindInventoryItemOrThrow(inventoryItemid, cancellationToken);
            inventoryItem.Delete();
            await _genericRepository.UpdateAsync(inventoryItem, cancellationToken, true);

            InvalidateCache();
            return true;
        }

        private async Task<InventoryItem> FindInventoryItemOrThrow(Guid inventoryItemId, CancellationToken cancellationToken, bool include = false)
        {
            InventoryItem inventoryItem = await _genericRepository.GetByIdAsync<InventoryItem>(
                inventoryItemId,
                cancellationToken,
                includes: include ? new string[]
                {
                    nameof(InventoryItem.Product),
                    nameof(InventoryItem.Warehouse)
                } : null);

            if (inventoryItem == null)
            {
                throw new InvalidOperationException("");
            }

            return inventoryItem;
        }

        private void InvalidateCache()
        {
            // This is definitely is not the way to handle cache invalidation but for time sake :(
            _memoryCache.Remove(CACHE_KEY);
            _memoryCache.Remove(REPORT_CACHE_KEY);
        }
    }
}
