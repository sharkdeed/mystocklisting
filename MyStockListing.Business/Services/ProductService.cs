﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.Extensions.Caching.Memory;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Data.Models;
using MyStockListing.Data.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Business.Services
{
    public sealed class ProductService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IGenericRepository _genericRepository;

        private const string CACHE_KEY = nameof(Product);

        public ProductService(IGenericRepository genericRepository, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _genericRepository = genericRepository;
        }

        public IQueryable<ProductOutputDTO> GetProducts(DynamicQueryOptions options)
        {
            return _memoryCache.GetOrCreate(CACHE_KEY, entry => _genericRepository
                .GetAll<Product>()
                .Select(ProductOutputDTO.Projection)
                .ApplyFilters(options).ToList()).AsQueryable();
        }

        public async Task<ProductOutputDTO> GetProductByIdAsync(Guid productId, CancellationToken cancellationToken)
        {
            Product product = await FindProductOrThrow(productId, cancellationToken);
            return ProductOutputDTO.FromEntity(product);

        }

        public async Task<ProductOutputDTO> CreateProductAsync(ProductInputDTO productInputDTO, CancellationToken cancellationToken)
        {
            var product = new Product(Guid.NewGuid(), productInputDTO.Name, productInputDTO.Brand, productInputDTO.Model, productInputDTO.SKU);
            await _genericRepository.AddAsync(product, cancellationToken, true);
            InvalidateCache();
            return ProductOutputDTO.FromEntity(product);
        }

        public async Task<ProductOutputDTO> UpdateProductAsync(Guid productId, ProductInputDTO productInputDTO, CancellationToken cancellationToken)
        {
            Product product = await FindProductOrThrow(productId, cancellationToken);
            product.ChangeMetadata(productInputDTO.Name, productInputDTO.Brand, productInputDTO.Model, productInputDTO.SKU);
            await _genericRepository.UpdateAsync(product, cancellationToken, true);
            InvalidateCache();
            return ProductOutputDTO.FromEntity(product);
        }

        public async Task<bool> DeleteProductAsync(Guid productId, bool isHardDelete, CancellationToken cancellationToken)
        {
            Product product = await FindProductOrThrow(productId, cancellationToken);

            // Check if we have any inventory items defined to the product.
            if (product.InventoryItems.Any())
            {
                throw new InvalidOperationException("Product still has got Inventory Items defined to it.");
            }

            // Soft delete operation
            product.Delete();
            await _genericRepository.UpdateAsync(product, cancellationToken, true);

            InvalidateCache();
            return true;
        }

        private async Task<Product> FindProductOrThrow(Guid productId, CancellationToken cancellationToken)
        {
            Product product = await _genericRepository.GetByIdAsync<Product>(productId, cancellationToken);
            if (product == null)
            {
                // not found exception
                throw new InvalidOperationException("");
            }

            return product;
        }

        private void InvalidateCache()
        {
            _memoryCache.Remove(CACHE_KEY);
        }
    }
}
