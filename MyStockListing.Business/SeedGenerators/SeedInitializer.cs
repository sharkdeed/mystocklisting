﻿using MyStockListing.Data.DataAccess;
using MyStockListing.Data.Models;
using MyStockListing.Data.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Business.SeedGenerators
{
    public class SeedInitializer
    {
        private readonly MyStockListingDbContext _dbContext;

        public SeedInitializer(MyStockListingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Seed()
        {
            IEnumerable<Country> countries = GenerateCountries().ToList();
            _dbContext.Countries.AddRange(countries);
            _dbContext.SaveChanges();

            IEnumerable<City> cities = GenerateCities(countries.ElementAt(0)).ToList();
            _dbContext.Cities.AddRange(cities);
            _dbContext.SaveChanges();

            City istanbul = cities.ElementAt(1);
            IEnumerable<District> districts = GenerateDistricts(cities.ElementAt(0), istanbul).ToList();
            _dbContext.Districts.AddRange(districts);
            _dbContext.SaveChanges();

            Location istanbulLocation = new Location(Guid.NewGuid(), districts.ElementAt(0), new Coordinates(1.1d, 1.1d));
            _dbContext.Locations.Add(istanbulLocation);
            _dbContext.SaveChanges();

            Warehouse istWh = new Warehouse(Guid.NewGuid(), istanbulLocation, "WH-IST");
            _dbContext.Warehouses.Add(istWh);
            _dbContext.SaveChanges();
        }

        public IEnumerable<City> GenerateCities(Country masterCountry)
        {
            yield return new City(Guid.NewGuid(), masterCountry, "Ankara", "ANK", "312");
            yield return new City(Guid.NewGuid(), masterCountry, "İstanbul", "İST", "212");
        }


        public IEnumerable<Country> GenerateCountries()
        {
            yield return new Country(Guid.NewGuid(), "Turkey", "TR");
        }

        public IEnumerable<District> GenerateDistricts(City ankara, City istanbul)
        {
            yield return new District(Guid.NewGuid(), istanbul, "Beşiktaş", "34353");
            yield return new District(Guid.NewGuid(), istanbul, "Maltepe", "34323");
            yield return new District(Guid.NewGuid(), istanbul, "Üsküdar", "34321");
            yield return new District(Guid.NewGuid(), ankara, "Eryaman", "06580");
            yield return new District(Guid.NewGuid(), ankara, "Kızılay", "06579");
            yield return new District(Guid.NewGuid(), ankara, "Çankaya", "06578");
        }
    }
}
