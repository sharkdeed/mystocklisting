﻿using MyStockListing.Data.ValueObjects;
using MyStockListing.Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Data.Models
{
    public class Warehouse : Entity
    {
        internal HashSet<InventoryItem> _inventoryItems = new HashSet<InventoryItem>();

        protected Warehouse() { }

        public string Name { get; private set; }

        public Guid LocationId { get; private set; }

        public virtual Location Location { get; private set; }

        public IReadOnlyCollection<InventoryItem> InventoryItems => _inventoryItems.ToList();

        public Warehouse(Guid id, Location location, string name)
            : base(id)
        {
            this.Name = name;
            this.LocationId = location.Id;
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Warehouse)}Created", this, DateTime.UtcNow));
        }

        public Location MoveToNewLocation(District district, Coordinates coordinates)
        {
            var newLocation = new Location(Guid.NewGuid(), district, coordinates);
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Warehouse)}MovedToNewLocation", new { LocationId = this.LocationId, NewLocationId = newLocation.Id }, DateTime.UtcNow));
            return new Location(Guid.NewGuid(), district, coordinates);
        }

        public void ChangeName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Warehouse)}NameChanged", new { this.Name, NewName = name }, DateTime.UtcNow));
                this.Name = name;
            }
        }
    }
}
