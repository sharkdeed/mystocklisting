﻿using MyStockListing.Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Data.Models
{
    public class City : Entity
    {
        protected City() { }

        internal readonly HashSet<District> _districts = new HashSet<District>();

        public string Name { get; private set; }

        public string ShortName { get; private set; }

        public string AreaCode { get; private set; }

        public Guid CountryId { get; private set; }

        public virtual Country Country { get; private set; }

        public virtual IReadOnlyCollection<District> Districts { get => _districts.ToList(); }

        public City(Guid id, Country country, string name, string shortName, string areaCode)
            : base(id)
        {
            this.Name = name;
            this.AreaCode = areaCode;
            this.ShortName = shortName;
            this.CountryId = country.Id;
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(City)}Created", this, DateTime.UtcNow));
        }
    }
}
