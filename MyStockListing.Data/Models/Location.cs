﻿using MyStockListing.Data.ValueObjects;
using MyStockListing.Infrastructure.Base;
using NetTopologySuite.Geometries;
using System;

namespace MyStockListing.Data.Models
{
    public class Location : Entity
    {
        protected Location() { }

        public Guid DistrictId { get; private set; }

        public virtual District District { get; private set; }

        public Point Coordinates { get; private set; }

        public Location(Guid id, District district, Coordinates coordinates)
            : base(id)
        {
            this.DistrictId = district.Id;
            this.Coordinates = new Point(coordinates.Longitude, coordinates.Latitude)
            {
                SRID = 4326
            };

            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Location)}Created", this, DateTime.UtcNow));
        }
    }
}
