﻿using MyStockListing.Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Data.Models
{
    public class Country : Entity
    {
        protected Country() { }

        internal readonly HashSet<City> _cities = new HashSet<City>();

        public string Name { get; private set; }

        public string ShortName { get; private set; }

        public virtual IReadOnlyCollection<City> Cities { get => _cities.ToList(); }

        public Country(Guid id, string name, string shortName)
            : base(id)
        {
            this.Name = name;
            this.ShortName = shortName;
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Country)}Created", this, DateTime.UtcNow));
        }
    }
}
