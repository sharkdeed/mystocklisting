﻿using MyStockListing.Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStockListing.Data.Models
{
    public sealed class Product : Entity
    {
        protected Product() { }

        internal HashSet<InventoryItem> _inventoryItems = new HashSet<InventoryItem>();

        public string Name { get; private set; }

        public string Brand { get; private set; }

        public string Model { get; private set; }

        public string SKU { get; private set; }

        public IReadOnlyCollection<InventoryItem> InventoryItems { get => _inventoryItems.ToList(); }

        public Product(Guid id, string name, string brand, string model, string sku)
            : base(id)
        {
            AppendMetadata(name, brand, model, sku);
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Product)}Created", this, DateTime.UtcNow));
        }

        public void ChangeMetadata(string name, string brand, string model, string sku)
        {
            AppendMetadata(name, brand, model, sku);
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(Product)}MetaDataChanged", this, DateTime.UtcNow));
        }

        private void AppendMetadata(string name, string brand, string model, string sku)
        {
            this.Name = name;
            this.Brand = brand;
            this.Model = model;
            this.SKU = sku;
        }
    }
}
