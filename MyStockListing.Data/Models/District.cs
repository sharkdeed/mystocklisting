﻿using MyStockListing.Infrastructure.Base;
using System;

namespace MyStockListing.Data.Models
{
    public class District : Entity
    {
        protected District() { }

        public string Name { get; private set; }

        public string PostalCode { get; private set; }

        public Guid CityId { get; private set; }

        public virtual City City { get; private set; }

        public District(Guid id, City city, string name, string postalCode)
            : base(id)
        {
            this.Name = name;
            this.CityId = city.Id;
            this.PostalCode = postalCode;
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(District)}Created", this, DateTime.UtcNow));
        }
    }
}
