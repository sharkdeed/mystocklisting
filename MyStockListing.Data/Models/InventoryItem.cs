﻿using MyStockListing.Infrastructure.Base;
using System;

namespace MyStockListing.Data.Models
{
    public class InventoryItem : Entity
    {
        protected InventoryItem() { }

        public int? Amount { get; private set; }

        public Guid ProductId { get; private set; }

        public Guid WarehouseId { get; private set; }

        public virtual Warehouse Warehouse { get; private set; }

        public virtual Product Product { get; private set; }

        public string SKU { get; private set; }

        public InventoryItem(
            Guid id,
            int? amount,
            Product product,
            Warehouse warehouse)
            : base(id)
        {
            Adjust(amount);
            this.ProductId = product.Id;
            this.WarehouseId = warehouse.Id;
            this.SKU = product.SKU;
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(InventoryItem)}Created", this, DateTime.UtcNow));
        }

        public void Adjust(int? adjustedAmount)
        {
            if (adjustedAmount.HasValue && adjustedAmount < 0)
            {
                throw new InvalidOperationException("You cannot set less than zero");
            }

            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(InventoryItem)}AmountAdjusted", new { Amount = this.Amount.GetValueOrDefault(), NewAmount = adjustedAmount }, DateTime.UtcNow));
            this.Amount = adjustedAmount;
        }

        public void MoveToNewWarehouse(Warehouse movedWarehouse)
        {
            this.Warehouse = movedWarehouse ?? throw new InvalidOperationException("Warehouse not found");
            base.Entries.Add(new EntryLog(Guid.NewGuid(), $"{nameof(InventoryItem)}WarehouseChanged", new { this.WarehouseId, NewWarehouseId = this.WarehouseId }, DateTime.UtcNow));
            this.WarehouseId = movedWarehouse.Id;
        }
    }
}
