﻿using Microsoft.EntityFrameworkCore;
using MyStockListing.Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Data.Repositories
{
    public interface IGenericRepository
    {
        IQueryable<TEntity> GetAll<TEntity>(bool includeSoftDeleted = false)
            where TEntity : Entity;

        IQueryable<TEntity> GetAllAsNoTracking<TEntity>(bool includeSoftDeleted = false)
            where TEntity : Entity;

        Task<TEntity> GetByIdAsync<TEntity>(Guid id, CancellationToken cancellationToken, string[] includes = null, bool includeSoftDeleted = false)
            where TEntity : Entity;

        Task<int> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;

        Task<int> AddRangeAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;

        void Detach<TEntity>(TEntity entity)
            where TEntity : Entity;

        Task<int> RemoveAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;

        Task<int> RemoveRangeAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;

        Task<int> UpdateRangeAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;

        Task<int> UpdateAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity;
    }

    public class GenericRepository : IGenericRepository
    {
        protected readonly DbContext _baseDb;

        public GenericRepository(DbContext baseDb)
        {
            _baseDb = baseDb;
        }

        public virtual IQueryable<TEntity> GetAll<TEntity>(bool includeSoftDeleted = false)
            where TEntity : Entity
        {
            IQueryable<TEntity> dataSet = _baseDb.Set<TEntity>();
            if (!includeSoftDeleted)
            {
                dataSet = dataSet.Where(x => x.DeletedAt == null);
            }

            return dataSet;
        }

        public virtual async Task<TEntity> GetByIdAsync<TEntity>(Guid id, CancellationToken cancellationToken, string[] includes = null, bool includeSoftDeleted = false)
            where TEntity : Entity
        {
            var set = GetAll<TEntity>(includeSoftDeleted);
            if (includes != null)
            {
                foreach (var item in includes)
                {
                    set = set.Include(item);
                }
            }

            return await set.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public virtual IQueryable<TEntity> GetAllAsNoTracking<TEntity>(bool includeSoftDeleted = false)
            where TEntity : Entity
        {
            return GetAll<TEntity>(includeSoftDeleted).AsNoTracking();
        }

        public virtual async Task<int> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
                where TEntity : Entity
        {
            int resultCount = 0;
            await _baseDb.Set<TEntity>().AddAsync(entity, cancellationToken);
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }

        public virtual async Task<int> AddRangeAsync<TEntity>(
            IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
                where TEntity : Entity
        {
            int resultCount = 0;
            await _baseDb.Set<TEntity>().AddRangeAsync(entities, cancellationToken);
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }

        public virtual void Detach<TEntity>(TEntity entity)
            where TEntity : Entity
        {
            _baseDb.Entry(entity).State = EntityState.Detached;
        }

        public virtual async Task<int> RemoveAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity
        {
            int resultCount = 0;
            _baseDb.Entry(entity).State = EntityState.Deleted;
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }

        public virtual async Task<int> RemoveRangeAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity
        {
            int resultCount = 0;
            _baseDb.Set<TEntity>().RemoveRange(entities);
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }

        public virtual async Task<int> UpdateRangeAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity
        {
            int resultCount = 0;
            foreach (TEntity entity in entities)
            {
                entity.Update();
            }

            _baseDb.Set<TEntity>().UpdateRange(entities);
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }

        public virtual async Task<int> UpdateAsync<TEntity>(TEntity entity, CancellationToken cancellationToken, bool saveChanges = false)
            where TEntity : Entity
        {
            int resultCount = 0;
            entity.Update();
            this._baseDb.Entry(entity).State = EntityState.Modified;
            this._baseDb.ChangeTracker.DetectChanges();
            if (saveChanges)
            {
                resultCount = await _baseDb.SaveChangesAsync(cancellationToken);
            }

            return resultCount;
        }
    }
}
