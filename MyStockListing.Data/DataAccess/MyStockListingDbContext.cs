﻿using Microsoft.EntityFrameworkCore;
using MyStockListing.Data.Models;

namespace MyStockListing.Data.DataAccess
{
    public class MyStockListingDbContext : DbContext
    {
        public const string DEFAULT_DECIMAL_PRECISION = "decimal(18, 2)";

        public MyStockListingDbContext(DbContextOptions<MyStockListingDbContext> options)
            : base(options)
        {
            Database.AutoTransactionsEnabled = false;
            ChangeTracker.LazyLoadingEnabled = false;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Product>(product =>
            {
                // Aynı Stok koduna sahip iki kayıt olmamalıdır. (Veritabanı validasyonu veya application validasyonu olmalıdır)
                product.HasIndex(prop => prop.SKU).IsUnique();
            });

            builder.Entity<Country>(country =>
            {
                country.Metadata.FindNavigation(nameof(Country.Cities)).SetPropertyAccessMode(PropertyAccessMode.Field);
            });

            builder.Entity<City>(city =>
            {
                city.Metadata.FindNavigation(nameof(City.Districts)).SetPropertyAccessMode(PropertyAccessMode.Field);
            });

            builder.Entity<Warehouse>(warehouse =>
            {
                warehouse.Metadata.FindNavigation(nameof(Warehouse.InventoryItems)).SetPropertyAccessMode(PropertyAccessMode.Field);
            });

            base.OnModelCreating(builder);
        }

        public virtual DbSet<City> Cities { get; private set; }

        public virtual DbSet<Country> Countries { get; private set; }

        public virtual DbSet<District> Districts { get; private set; }

        public virtual DbSet<InventoryItem> InventoryItems { get; private set; }

        public virtual DbSet<Product> Products { get; private set; }

        public virtual DbSet<Warehouse> Warehouses { get; private set; }

        public virtual DbSet<Location> Locations { get; private set; }

    }
}
