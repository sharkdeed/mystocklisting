﻿using MyStockListing.Infrastructure.Base;

namespace MyStockListing.Data.ValueObjects
{
    public class DimensionalUnit : ValueObject<DimensionalUnit>
    {
        public DimensionalUnit()
        {

        }

        public string Unit { get; private set; }

        // Considering the fact that its a PoC project, please excuse my avoidance of precision handling here :)
        public decimal Value { get; private set; }

        public DimensionalUnit(string unit, decimal value)
        {
            this.Unit = unit;
            this.Value = value;
        }
    }
}
