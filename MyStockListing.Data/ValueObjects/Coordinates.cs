﻿using MyStockListing.Infrastructure.Base;

namespace MyStockListing.Data.ValueObjects
{
    public class Coordinates : ValueObject<Coordinates>
    {
        public Coordinates()
        {

        }

        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        public Coordinates(double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
    }
}
