﻿using DynamicQueryBuilder.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyStockListing.Business.Services;
using MyStockListing.Data.DataAccess;
using MyStockListing.Data.Repositories;

namespace MyStockListing.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMemoryCache();
            services.AddDbContext<MyStockListingDbContext>(opts =>
            {
                opts.UseSqlServer("Server=.;Database=mystocklisting-db;Trusted_Connection=True;", sqlOptions =>
                {
                    sqlOptions.UseNetTopologySuite();
                    sqlOptions.CommandTimeout(120);
                });

                opts.UseMemoryCache(services.BuildServiceProvider().GetService<IMemoryCache>());
            });

            services.AddSingleton(new DynamicQueryBuilderSettings
            {
                IgnorePredefinedOrders = true,
                UsesCaseInsensitiveSource = true
            });


            services.AddScoped<DbContext, MyStockListingDbContext>();
            services.AddScoped<IGenericRepository, GenericRepository>();
            services.AddScoped<ProductService>();
            services.AddScoped<InventoryItemService>();
            services.AddScoped<WarehouseService>();
            services.AddScoped<LocationService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
