﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

const ENTER_KEY = 13;

$("#cityList").on("change", () => {
    $.ajax({
        type: "GET",
        url: `/Locations/GetDistrictsByCityId?cityId=` + $("#cityList").val(),
        success: function (data) {
            var s = '<option value="-1"></option>';
            for (var i = 0; i < data.length; i++) {
                s += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            $("#districtList").html(s);
        }
    });
});


function submitProductFilters() {
    const productNameTxt = $("#txtProductName").val();
    const dtMinDate = $("#dtMinDate").val();
    const dtMaxDate = $("#dtMaxDate").val();

    let queryString = "";
    if (productNameTxt !== '') {
        queryString += `o=contains&p=name&v=${productNameTxt}`;
    }

    queryString += dtMinDate
        ? `o=gtoe&p=createdat&v=` + dtMinDate
        : '';

    queryString += dtMaxDate
        ? `&o=ltoe&p=createdat&v=` + dtMaxDate
        : '';

    $.ajax({
        type: "GET",
        url: `/Products/IndexGrid?` + encodeURIComponent(queryString),
        success: function (data) {
            $("#productsArea").html(data);
        }
    });
}

function submitWarehouseFilters() {
    const longitude = $("#txtLongitude").val();
    const latitude = $("#txtLatitude").val();

    $.ajax({
        type: "GET",
        url: `/Warehouses/IndexGrid?longitude=${longitude}&latitude=${latitude}`,
        success: function (data) {
            $("#warehousesArea").html(data);
        }
    });
}