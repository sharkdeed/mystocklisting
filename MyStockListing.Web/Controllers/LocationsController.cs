﻿using Microsoft.AspNetCore.Mvc;
using MyStockListing.Business.Services;
using System;

namespace MyStockListing.Web.Controllers
{
    public class LocationsController : Controller
    {
        private readonly LocationService _locationService;

        public LocationsController(LocationService locationService)
        {
            _locationService = locationService;
        }

        public IActionResult GetDistrictsByCityId(Guid cityId)
        {
            return Ok(_locationService.GetDistrictsByCityId(cityId));
        }
    }
}