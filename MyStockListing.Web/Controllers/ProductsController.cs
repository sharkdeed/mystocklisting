﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.AspNetCore.Mvc;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Business.Services;
using MyStockListing.Web.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ProductService _productService;

        public ProductsController(ProductService productService)
        {
            _productService = productService;
        }

        // GET: Products
        [DynamicQuery]
        public IActionResult Index()
        {
            return View();
        }

        // GET: Products
        [DynamicQuery]
        public IActionResult IndexGrid(DynamicQueryOptions dynamicQueryOptions)
        {
            IQueryable<ProductOutputDTO> products = _productService.GetProducts(dynamicQueryOptions);

            return PartialView("_genericGrid", new GenericGridModel
            {
                CRUDActions = true,
                GridId = $"grid-{nameof(products)}",
                Items = products,
                SetName = nameof(products),
                Columns = new string[]
                {
                    nameof(ProductOutputDTO.SKU),
                    nameof(ProductOutputDTO.Name),
                    nameof(ProductOutputDTO.Brand),
                    nameof(ProductOutputDTO.Model),
                    nameof(ProductOutputDTO.CreatedAt)
                }
            });
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _productService.GetProductByIdAsync(id.Value, cancellationToken);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Brand,Model,SKU")] ProductInputDTO product, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                ProductOutputDTO result = await _productService.CreateProductAsync(product, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _productService.GetProductByIdAsync(id.Value, cancellationToken);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Brand,Model,SKU")] ProductInputDTO product, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                await _productService.UpdateProductAsync(id, product, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _productService.GetProductByIdAsync(id.Value, cancellationToken);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id, CancellationToken cancellationToken)
        {
            await _productService.DeleteProductAsync(id, false, cancellationToken);
            return RedirectToAction(nameof(Index));
        }
    }
}
