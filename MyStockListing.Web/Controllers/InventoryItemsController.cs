﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Business.Services;
using MyStockListing.Web.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Web.Controllers
{
    public class InventoryItemsController : Controller
    {
        private readonly ProductService _productService;
        private readonly InventoryItemService _inventoryItemService;
        private readonly WarehouseService _warehouseService;

        public InventoryItemsController(
            InventoryItemService inventoryItemService,
            ProductService productService,
            WarehouseService warehouseService)
        {
            _productService = productService;
            _inventoryItemService = inventoryItemService;
            _warehouseService = warehouseService;
        }

        // GET: InventoryItems
        public IActionResult Index(DynamicQueryOptions dynamicQueryOptions)
        {
            return View();
        }

        // GET: IndexGrid
        [DynamicQuery]
        public IActionResult IndexGrid(DynamicQueryOptions dqbOptions)
        {
            IQueryable<InventoryItemOutputDTO> inventoryItems = _inventoryItemService.GetInventoryItems(dqbOptions);

            return PartialView("_genericGrid", new GenericGridModel
            {
                CRUDActions = true,
                GridId = $"grid-{inventoryItems}",
                Items = inventoryItems,
                SetName = nameof(inventoryItems),
                Columns = new string[]
                {
                    nameof(InventoryItemOutputDTO.Amount),
                    $"{nameof(InventoryItemOutputDTO.ProductName)}",
                    $"{nameof(InventoryItemOutputDTO.Model)}",
                    $"{nameof(InventoryItemOutputDTO.SKU)}",
                    $"{nameof(InventoryItemOutputDTO.WarehouseName)}"
                }
            });
        }

        // GET: InventoryItems/Details/5
        public async Task<IActionResult> Details(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _inventoryItemService.GetInventoryItemByIdAsync(id.Value, cancellationToken);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        // GET: InventoryItems/Create
        public IActionResult Create()
        {
            SetViewDataWithProductsAndWarehouses(null, null);
            return View();
        }

        // POST: InventoryItems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Amount,ReservedAmount,ProductId,WarehouseId")] InventoryItemInputDTO inventoryItem, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                await _inventoryItemService.CreateInventoryItemAsync(inventoryItem, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            return View(inventoryItem);
        }

        // GET: InventoryItems/Edit/5
        public async Task<IActionResult> Edit(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _inventoryItemService.GetInventoryItemByIdAsync(id.Value, cancellationToken);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            SetViewDataWithProductsAndWarehouses(inventoryItem.Product.Id, inventoryItem.Warehouse.Id);
            return View(inventoryItem);
        }

        // POST: InventoryItems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Amount,ProductId,WarehouseId")] InventoryItemInputDTO inventoryItem, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                await _inventoryItemService.UpdateInventoryItem(id, inventoryItem, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            SetViewDataWithProductsAndWarehouses(inventoryItem.ProductId, inventoryItem.WarehouseId);
            return View(inventoryItem);
        }

        // GET: InventoryItems/Delete/5
        public async Task<IActionResult> Delete(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _inventoryItemService.GetInventoryItemByIdAsync(id.Value, cancellationToken);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        // POST: InventoryItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id, CancellationToken cancellationToken)
        {
            await _inventoryItemService.DeleteProductAsync(id, false, cancellationToken);
            return RedirectToAction(nameof(Index));
        }

        private void SetViewDataWithProductsAndWarehouses(Guid? selectedProductId, Guid? selectedWarehouseId)
        {
            ViewData["ProductId"] = new SelectList(_productService.GetProducts(null), "Id", "Name", selectedProductId);
            ViewData["WarehouseId"] = new SelectList(_warehouseService.GetWarehouses(null, 0, 0), "Id", "Name", selectedWarehouseId);
        }
    }
}
