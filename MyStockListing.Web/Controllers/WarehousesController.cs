﻿using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyStockListing.Business.DTOs.Input;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Business.Services;
using MyStockListing.Web.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStockListing.Web.Controllers
{
    public class WarehousesController : Controller
    {
        private readonly LocationService _locationService;
        private readonly WarehouseService _warehouseService;

        public WarehousesController(WarehouseService warehouseService, LocationService locationService)
        {
            _locationService = locationService;
            _warehouseService = warehouseService;
        }

        // GET: Warehouses
        [DynamicQuery]
        public IActionResult Index(DynamicQueryOptions dynamicQueryOptions)
        {
            return View(_warehouseService.GetWarehouses(dynamicQueryOptions, 0, 0));
        }

        // GET: Warehouses
        [DynamicQuery]
        public IActionResult IndexGrid(DynamicQueryOptions dqbOptions, double longitude, double latitude)
        {
            IQueryable<WarehouseOutputDTO> warehouses = _warehouseService.GetWarehouses(dqbOptions, longitude, latitude);

            return PartialView("_genericGrid", new GenericGridModel
            {
                CRUDActions = true,
                GridId = $"grid-{warehouses}",
                Items = warehouses,
                SetName = nameof(warehouses),
                Columns = new string[]
                {
                    nameof(WarehouseOutputDTO.Name),
                    nameof(WarehouseOutputDTO.Address),
                    nameof(WarehouseOutputDTO.Coordinates)
                }
            });
        }

        // GET: Warehouses/Details/5
        public async Task<IActionResult> Details(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _warehouseService.GetWarehouseByIdAsync(id.Value, cancellationToken);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // GET: Warehouses/Create
        public IActionResult Create()
        {
            var countries = _locationService.GetAllCountries().ToList();
            ViewData["countries"] = new SelectList(countries, "Id", "Name");
            ViewData["cities"] = new SelectList(countries.SelectMany(x => x.Cities), "Id", "Name");
            return View();
        }

        // POST: Warehouses/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,DistrictId,Coordinates,Latitude,Longitude")] WarehouseInputDTO warehouse, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                await _warehouseService.CreateWarehouseAsync(warehouse, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            return View(warehouse);
        }

        // GET: Warehouses/Edit/5
        public async Task<IActionResult> Edit(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _warehouseService.GetWarehouseByIdAsync(id.Value, cancellationToken);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // POST: Warehouses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,DistrictId,Coordinates")] WarehouseInputDTO warehouse, CancellationToken cancellationToken)
        {
            if (ModelState.IsValid)
            {
                await _warehouseService.UpdateWarehouseAsync(id, warehouse, cancellationToken);
                return RedirectToAction(nameof(Index));
            }

            return View(warehouse);
        }

        // GET: Warehouses/Delete/5
        public async Task<IActionResult> Delete(Guid? id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _warehouseService.GetWarehouseByIdAsync(id.Value, cancellationToken);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // POST: Warehouses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id, CancellationToken cancellationToken)
        {
            await _warehouseService.DeleteProductAsync(id, false, cancellationToken);
            return RedirectToAction(nameof(Index));
        }
    }
}
