﻿using Microsoft.AspNetCore.Mvc;
using MyStockListing.Business.DTOs.Output;
using MyStockListing.Business.Services;
using MyStockListing.Web.Models;
using System.Diagnostics;
using System.Linq;

namespace MyStockListing.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly InventoryItemService _inventoryItemService;

        public HomeController(InventoryItemService inventoryItemService)
        {
            _inventoryItemService = inventoryItemService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult StockReportGrid()
        {
            IQueryable<StockReportOutputDTO> stockReports = _inventoryItemService.GetStockReport();
            return PartialView("_genericGrid", new GenericGridModel
            {
                CRUDActions = false,
                GridId = $"grid-{nameof(stockReports)}",
                Items = stockReports,
                SetName = nameof(stockReports),
                Columns = new string[]
                {
                    nameof(StockReportOutputDTO.SKU),
                    nameof(StockReportOutputDTO.TotalAmount),
                    nameof(StockReportOutputDTO.Brand),
                    nameof(StockReportOutputDTO.Name),
                    nameof(StockReportOutputDTO.Model),
                    nameof(StockReportOutputDTO.Warehouse)
                }
            });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
