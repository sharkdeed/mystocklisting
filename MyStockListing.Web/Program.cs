﻿using EFCoreAutoMigrator;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MyStockListing.Business.SeedGenerators;
using MyStockListing.Data.DataAccess;

namespace MyStockListing.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost server = BuildWebHost(args);
            var env = server.Services.GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;
            if (!env.IsProduction())
            {
                using (IServiceScope serviceScope = server.Services.GetService<IServiceScopeFactory>().CreateScope())
                {
                    MyStockListingDbContext myStockListingDbContext = serviceScope.ServiceProvider.GetService<MyStockListingDbContext>();
                    new AutoMigrator(myStockListingDbContext)
                        .EnableAutoMigration(false, MigrationModelHashStorageMode.Database, () =>
                        {
                            new SeedInitializer(myStockListingDbContext).Seed();
                        });
                }
            }

            server.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
