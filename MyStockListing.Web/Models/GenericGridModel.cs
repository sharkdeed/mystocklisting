﻿using System.Collections.Generic;

namespace MyStockListing.Web.Models
{
    public class GenericGridModel
    {
        public string GridId { get; set; }

        public IEnumerable<object> Items { get; set; }

        public bool CRUDActions { get; set; }

        public string SetName { get; set; }

        public string[] Columns { get; set; }
    }
}
